from patterns_python.abstract_factory.loggers import app_logger


# Concrete Urgent Notification
class UrgentNotification:

    def notify(self):
        app_logger.warning("There is an urgent notification")
