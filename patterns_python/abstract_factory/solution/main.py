from patterns_python.abstract_factory.solution.factories import (
    InternalNotificationFactory,
    ExternalNotificationFactory,
    NotificationFactory
)
from patterns_python.abstract_factory.loggers import app_logger
from patterns_python.abstract_factory.solution.repository import in_memory_db


class Client:

    def __init__(self, client_id: str, notifications: NotificationFactory):
        self.id = client_id
        self.daily_notifications = notifications.create_daily_notification()
        self.urgent_notifications = notifications.create_urgent_notification()

    def offer_client(self):
        self.urgent_notifications.notify()

    def queue_order(self):
        self.daily_notifications.notify()

    def process_daily_notifications(self):
        self.daily_notifications.dispatch()

    def process(self, op: str):
        if op == "1":
            self.offer_client()
        elif op == "2":
            self.queue_order()
        elif op == "3":
            self.process_daily_notifications()
        else:
            app_logger.info("Nothing to do")


def myapp():
    # security_group = ["javier"]
    client_id = input("Insert client id: ")

    if client_id in in_memory_db:
        notifications = InternalNotificationFactory()
        app_logger.info("Internal client")
    # elif client_id in in_memory_db and client_id in security_group:
    #     notifications = CypheredNotificationFactory()
    #     app_logger.info("Secret client")
    else:
        notifications = ExternalNotificationFactory()
        app_logger.info("External client")

    my_client = Client(
        client_id=client_id,
        notifications=notifications
    )

    while True:
        op = input(
            """Select operation number:
                [1] get an offer
                [2] queue an offer
                [3] dispatch offers of the day
                [x] exit
            \r>> """
        )

        if op == "x":
            exit(0)
        else:
            my_client.process(op)


if __name__ == "__main__":
    myapp()
