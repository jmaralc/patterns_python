from abc import ABC, abstractmethod
from datetime import datetime
from typing import Dict

from patterns_python.abstract_factory.loggers import (
    internal_logger,
    external_logger
)


# Abstract class
class DailyNotification(ABC):

    store: Dict[datetime, str]

    def __init__(self):
        super().__init__()
        self.store = {}

    @abstractmethod
    def notify(self):
        raise NotImplementedError("not yet")

    @abstractmethod
    def dispatch(self):
        raise NotImplementedError("not yet")


# Concrete Internal Daily Notification
class InternalDailyNotification(DailyNotification):
    def notify(self):
        self.store[datetime.now()] = "There is an internal notification"

    def dispatch(self):
        for date, message in self.store.items():
            if date < datetime.today():
                internal_logger.info(message)


# Concrete External Daily Notification
class ExternalDailyNotification(DailyNotification):
    def notify(self):
        self.store[datetime.now()] = "There is an internal notification"

    def dispatch(self):
        for date, message in self.store.items():
            if date < datetime.today():
                external_logger.info(message)
