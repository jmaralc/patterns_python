import time


from patterns_python.observer.solution.cryptoprice import (
    CryptoPrice
)
from patterns_python.observer.solution.figure import (
    ObserverFigure
)


def myapp():
    bitcoin_price = CryptoPrice("bitcoin")
    ethereum_price = CryptoPrice("ethereum")
    tether_price = CryptoPrice("tether")

    figure1 = ObserverFigure()
    figure2 = ObserverFigure()

    bitcoin_price.attach(figure1)
    ethereum_price.attach(figure1)

    bitcoin_price.attach(figure2)
    tether_price.attach(figure2)

    count = 0
    while True:
        # Sampling phase
        bitcoin_price.sample()
        ethereum_price.sample()
        tether_price.sample()

        time.sleep(1)

        count += 1
        if count == 10:
            tether_price.detach(figure2)
        if count == 10:
            bitcoin_price.detach(figure2)
        if count == 20:
            bitcoin_price.attach(figure2)


if __name__ == "__main__":
    myapp()
