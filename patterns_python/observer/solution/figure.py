import matplotlib.pyplot as plt

from patterns_python.observer.solution.observer import Observer


class ObserverFigure(Observer):

    def __init__(self):
        self.fig, self.axes = plt.subplots(2, 1)
        self.fig.suptitle('Crypto currency comparison', fontsize=16)

    def update(self, origin, values):
        if origin == "bitcoin":
            self.axes[0].clear()
            self.axes[0].plot(values)
            self.axes[0].set_title("bitcoin")
            self.axes[0].grid()
        else:
            self.axes[1].clear()
            self.axes[1].plot(values)
            self.axes[1].set_title(origin)
            self.axes[1].grid()

        plt.draw()
        plt.pause(0.001)
