from random import randint, choices


class Warrior:
    def __init__(self):
        self.stg = randint(12, 18)
        self.dex = randint(11, 16)
        self.con = randint(12, 16)
        self.int = randint(5, 14)
        self.wis = randint(8, 16)
        self.cha = randint(2, 18)

        self.weapon = choices(["sword", "battle axe", "bow"]).pop()
        self.armour = choices(
            ["chainmail", "plate armour", "leather armour"]
        ).pop()

    def __repr__(self):
        return f"""
        o--{{======> Your character  <======}}--o
            💪 Strength: {self.stg}
            🤸 Dexterity: {self.dex}
            🏋️ Constitution: {self.con}
            🧠 Intelligence: {self.int}
            🦉 Wisdom: {self.wis}
            🎭 Character: {self.cha}
            -------------------------
            ⚔️ Weapon: {self.weapon}
            🛡️ Armour: {self.armour}
        """


def myapp():
    character_type = input(
        """ Select the character you want to create:
            [1] Warrior
            [x] Exit
            \r>> """
    )

    character = None
    if character_type == "1":
        character = Warrior()
    else:
        print("Bye")

    if character is not None:
        print(character)


if __name__ == "__main__":
    myapp()
