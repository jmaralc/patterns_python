from patterns_python.builder.solution.character_builder import (
    WarriorBuilder,
    RangerBuilder
)


def myapp():
    character_type = input(
        """ Select the character you want to create:
            [1] Warrior
            [2] Ranger
            [x] Exit
        \r>> """
    )

    race = input(
        """ Select the character you want to create:
            [1] Human
            [2] Elf
            [3] Dwarf
            [x] Exit
        \r>> """
    )

    if race == "1":
        race = "human"
    elif race == "2":
        race = "elf"
    elif race == "3":
        race = "dwarf"
    else:
        exit(0)

    character = None
    if character_type == "1":
        character = WarriorBuilder
    elif character_type == "2":
        character = RangerBuilder
    else:
        exit(0)

    # character = character().base().race(race).from_mountains().build()
    character = character().base().from_mountains().build()

    if character is not None:
        print(character)


if __name__ == "__main__":
    myapp()
