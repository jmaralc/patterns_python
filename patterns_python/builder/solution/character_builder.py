from abc import ABC, abstractmethod
from random import randint, choices

from patterns_python.builder.solution.character import (
    Character
)


class CharacterBuilder(ABC):

    _character: Character

    @abstractmethod
    def base(self):
        raise NotImplementedError("Not yet")

    def build(self):
        return self._character

    def from_mountains(self):
        self._character.con += 1
        return self

    def race(self, race: str):
        if race == "elf":
            self._character.race = "Elf"
            self._character.dex += 1
            self._character.int += 1
            self._character.stg -= randint(1, 4)
        elif race == "dwarf":
            self._character.race = "Dwarf"
            self._character.con += 1
            self._character.stg += 1
            self._character.dex -= randint(1, 4)
        else:
            self._character.con += 1
            self._character.cha += 1
            self._character.wis -= randint(1, 4)
        return self


class WarriorBuilder(CharacterBuilder):

    def __init__(self):
        self._character = Character()

    def base(self):
        self._character.profession = "Warrior"
        self._character.stg = randint(12, 18)
        self._character.dex = randint(11, 16)
        self._character.con = randint(12, 16)
        self._character.int = randint(5, 14)
        self._character.wis = randint(8, 16)
        self._character.cha = randint(2, 18)

        self._character.weapon = choices(["sword", "battle axe", "bow"]).pop()

        self._character.armour = choices(
            ["chainmail", "plate armor", "leather armor"]
        ).pop()

        return self


class RangerBuilder(CharacterBuilder):

    def __init__(self):
        self._character = Character()

    def base(self):
        self._character.profession = "Ranger"
        self._character.stg = randint(8, 14)
        self._character.dex = randint(12, 18)
        self._character.con = randint(12, 18)
        self._character.int = randint(5, 14)
        self._character.wis = randint(10, 16)
        self._character.cha = randint(2, 12)

        self._character.weapon = choices(["bow", "dagger", "crossbow"]).pop()

        self._character.armour = choices(["fur armor", "leather armor"]).pop()

        return self
