from typing import List

from patterns_python.decorator.solution import prompt


class Shell:

    def __init__(
        self,
        shell_prompt: prompt.BasePrompt
    ):
        self.prompt = shell_prompt
        self.history: List = []
        self.last_command = None

    def input(self):
        self.last_command = input(self.prompt.print())
        self.history.append(self.last_command)

        self.execute()

    def execute(self):
        if self.last_command == "history":
            for index, command in enumerate(self.history):
                print(f"[{index}] {command}")
        if self.last_command == "exit":
            exit(0)


def myapp(host: bool = True, time: bool = False, color: bool = True):

    my_prompt: prompt.BasePrompt = prompt.BasicPrompt()

    if time:
        my_prompt = prompt.DatetimeDecorator(my_prompt)
    if host:
        my_prompt = prompt.HostDecorator(my_prompt)
    if color:
        my_prompt = prompt.ColorDecorator(my_prompt)

    my_shell = Shell(my_prompt)

    while True:
        my_shell.input()


if __name__ == "__main__":
    myapp(
        host=True,
        time=True,
    )
