from abc import ABC, abstractmethod
from datetime import datetime
import socket


# Interface that defines the contract with shell
class BasePrompt(ABC):
    @abstractmethod
    def print(self):
        raise NotImplementedError("Not yet")


# Concrete basic class of the previous interface
class BasicPrompt(BasePrompt):

    def __init__(self):
        self.format = ">>"

    def print(self):
        return self.format


# Interface that inherit from the BasePrompt
# so it respect and fulfill the contract with shell
class PromptDecorator(BasePrompt, ABC):

    def __init__(self, decorated_prompt: BasePrompt):
        self._decorated_prompt = decorated_prompt

    @abstractmethod
    def print(self):
        raise NotImplementedError("Not yet")


# Concrete implementation of the decorator
class DatetimeDecorator(PromptDecorator):
    def print(self):
        print_to_be_modified = self._decorated_prompt.print()
        return f"[{datetime.today()}]{print_to_be_modified}"


# Concrete implementation of the decorator
class HostDecorator(PromptDecorator):
    def print(self):
        print_to_be_modified = self._decorated_prompt.print()

        return f"@{socket.gethostname()}{print_to_be_modified}"


# Concrete implementation of the decorator
# As you can see the decorator can add much more functionality
class ColorDecorator(PromptDecorator):

    def __init__(
        self,
        decorated_prompt: BasePrompt,
        style: str = "1",
        foreground_color: str = "33",
        background_color: str = "40",
    ):
        super().__init__(decorated_prompt)
        self.format = ";".join([style, foreground_color, background_color])

    def print(self):
        print_to_be_modified = self._decorated_prompt.print()

        return f"\x1b[{self.format}m {print_to_be_modified} \x1b[0m"
